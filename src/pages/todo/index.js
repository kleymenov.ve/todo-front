import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Formik, Form, Field } from 'formik';
import APIHelper from '../../APIHelper';



const Todo = (props) => {
  const { id } = useParams();
  const history = useHistory();
  const [todo, setTodo] = useState({ task: '', description: '', user: '', completed: false });
  const isNew = id === 'new';

  useEffect(() => {
    const fetchData = async () => {
      return APIHelper.fetchTodo(id);
    }

    if (!isNew) {
      fetchData()
        .then((response) => {
          const { data } = response;
          setTodo(data);
        });
    }

  }, []);

  const createTodo = async (values) => {
    const newTodo = await APIHelper.createTodo(values);
    history.push(`/list/${newTodo._id}`);
  }

  const deleteTodo = async () => {
    try {
      await APIHelper.deleteTodo(id)
      history.push('/list');
    } catch (err) {}
  }

  const updateTodo = async (values) => {
    await APIHelper.updateTodo(id, values);
  }

  const submit = async (values) => {
    if (isNew) {
      createTodo(values).catch((err) => console.error(err));
    } else {
      updateTodo(values).catch((err) => console.error(err));
    }
  }

  return (
    <div>
      <button onClick={() => history.push('/list')}>{"<"} Назад</button>
      {!isNew && <button onClick={deleteTodo}>{'X'} Удалить</button>}
      <Formik
        onSubmit={submit}
        initialValues={todo}
        enableReinitialize
      >
        <Form>
          <Field name="task" />
          <Field name="description" />
          <Field name="user" />
          <Field name="completed" type="checkbox" />
          <button type="submit">Сохранить</button>
        </Form>
      </Formik>
	    {/*<form onSubmit={submit}>*/}
      {/*  <input name="task" onChange={handleInputChange} value={todo.task} /><br />*/}
      {/*  <input name="description" onChange={handleInputChange} value={todo.description} /><br />*/}
      {/*  <input name="user" onChange={handleInputChange} value={todo.user} /><br />*/}
      {/*  <input name="completed" onChange={handleInputChange} value={todo.completed} type="checkbox" /><br />*/}
      {/*</form>*/}
    </div>
  );
};

Todo.propTypes = {

};

export default Todo;