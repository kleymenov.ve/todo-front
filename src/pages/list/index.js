import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import PropTypes from 'prop-types';
import APIHelper from '../../APIHelper';

const TodoList = (props) => {
  const [todos, setTodos] = useState([]);
  const history = useHistory();

  useEffect(() => {
    const fetchTodoAndSetTodos = async () => {
      const todos = await APIHelper.getAllTodos();
      setTodos(todos);
    };
    fetchTodoAndSetTodos();
  }, []);


  const selectTodo = async (event, id) => {
    history.push(`/list/${id}`);
  };

  return (
    <div>
      <button onClick={() => history.push('/list/new')}>Создать</button>
      <ul>
        {todos.map(({ _id, task, completed }, i) => (
          <li
            key={i}
            onClick={e => selectTodo(e, _id)}
            className={`${completed ? "completed" : ""} list-item`}
          >
            {task}
          </li>
        ))}
      </ul>
    </div>
  );
};

TodoList.propTypes = {};

export default TodoList;