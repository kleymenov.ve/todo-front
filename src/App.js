import React, { useState, useEffect, useReducer } from 'react';
import './App.scss';
import APIHelper from './APIHelper.js';
import { Switch, Route, Redirect } from 'react-router';
import TodoList from './pages/list';
import Todo from './pages/todo';


function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/" exact>
          <Redirect to="/list" exact />
        </Route>
        <Route path="/list" exact component={TodoList} />
        <Route path="/list/:id" component={Todo} />
      </Switch>
    </div>
  );
}

export default App;